/*
 * @Author: chengpengfei
 * @Date: 2023-01-28 14:57:03
 * @LastEditors: chengpengfei
 * @LastEditTime: 2023-02-03 18:00:04
 * @Description: 
 */
extern crate winapi;
extern crate winres;
fn main() {
    let mut res = winres::WindowsResource::new();
    res.set_icon("./icon.ico");
    // res.set_language(winapi::um::winnt::MAKELANGID(
    //     winapi::um::winnt::LANG_CHINESE_SIMPLIFIED,
    //     winapi::um::winnt::SUBLANG_CHINESE_SIMPLIFIED,
    // ));
    res.set("ProductName", "hero");
    res.set("CompanyName", "cpf23333");
    res.compile().unwrap()
}
