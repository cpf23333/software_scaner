use serde::Deserialize;
use serde_json;

#[derive(Deserialize, Debug)]
struct JSONItem {
    /// 软件名称
    name: String,
    /// 公司名称
    company: String,
}
const DEFAULT_LIST: &str = include_str!("../assets/black_list.json");
pub fn software_checker_creater() -> impl Fn(Vec<String>) -> bool {
    let list: Vec<JSONItem> = serde_json::from_str(&DEFAULT_LIST).expect("解析失败");
    println!("{:?}", list);
    let mut unuseable_names = vec![];
    let mut unuseable_company = vec![];
    for json in list {
        unuseable_names.push(json.name);
        unuseable_company.push(json.company);
    }
    //返回值为软件是否在黑名单里
    let software_cannot_use = move |row: Vec<String>| -> bool {
        // return not_names.contains(&row[0]) || not_company.contains(&row[2]);
        let name_unuseable = unuseable_names.contains(&row[0]);
        let company_unuseable = match &row[2].len() > &0 {
            true => unuseable_company.contains(&row[2]),
            false => false,
        };
        return name_unuseable || company_unuseable;
    };
    return software_cannot_use;
}
