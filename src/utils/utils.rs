/*
 * @Author: chengpengfei
 * @Date: 2023-02-01 17:11:51
 * @LastEditors: chengpengfei
 * @LastEditTime: 2023-02-02 13:51:19
 * @Description:
 */
use wfd::{self, DialogParams};
/// 弹窗由用户选择文件保存位置及文件名，返回完整文件路径
pub fn get_save_file_path() -> String {
    let option = DialogParams {
        title: "软件扫描完毕请选择html文件存放位置",
        file_types: vec![("html文件", "*.html")],
        default_extension: "html",
        file_name: "软件清单",
        file_name_label: "文件名称",
        file_type_index: 1,
        ..Default::default()
    };
    return wfd::save_dialog(option)
        .expect("您已取消操作")
        .selected_file_path
        .to_str()
        .unwrap()
        .to_owned();
}
