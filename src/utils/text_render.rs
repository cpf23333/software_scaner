/*
 * @Author: chengpengfei
 * @Date: 2023-02-01 14:11:08
 * @LastEditors: chengpengfei
 * @LastEditTime: 2023-02-02 14:25:23
 * @Description:
 */
use fontdue::{
    self,
    layout::{CoordinateSystem, Layout, LayoutSettings, TextStyle},
    Font,
};

use super::get_font::get_font_data;
pub struct TextCanvas {
    font: Font,
}
impl TextCanvas {
    pub(crate) fn new() -> TextCanvas {
        let font = get_font_data();
        // Self.font = fontdue::Font::from_bytes(font, fontdue::FontSettings::default()).unwrap();
        TextCanvas {
            font: fontdue::Font::from_bytes(font, fontdue::FontSettings::default()).unwrap(),
        }
    }
    pub(crate) fn get_text_width(&self, text: String,) -> usize {
        let fonts = &[&self.font];
        let s = TextStyle::new(&text, 24.0, 0);
        let mut layout = Layout::new(CoordinateSystem::PositiveYUp);
        layout.reset(&LayoutSettings::default());
        layout.append(fonts, &s);
        let mut length = 0;
        for glyph in layout.glyphs() {
            // println!("{}",glyph.x);
            length += glyph.width;
        }
        // println!("length{},{:?}", length,layout.glyphs());
        return length;
    }
}
