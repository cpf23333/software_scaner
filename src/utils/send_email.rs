/*
 * @Author: chengpengfei
 * @Date: 2023-01-30 18:19:37
 * @LastEditors: chengpengfei
 * @LastEditTime: 2023-02-02 14:00:52
 * @Description:
 */
use mail_send::{self, SmtpClientBuilder, Credentials};

use super::ask_email_about::MailAddressData;

pub async fn send_email(pdf_data: Vec<u8>,email_address_data:&MailAddressData) {
    let email = mail_send::mail_builder::MessageBuilder::new()
        .from((email_address_data.sender_name.to_string(), email_address_data.address.to_string()))
        .to(vec![("NoBody", "3288765969@qq.com")])
        .subject("自定义的标题")
        .text_body("第一行内容\r\n第二行内容")
        .binary_attachment("application/pdf", "附件.pdf", pdf_data);
        let credentials:Credentials<&str>=Credentials::Plain { username: &email_address_data.address.to_string(), secret: &email_address_data.credential_code };
    SmtpClientBuilder::new(email_address_data.mail_server_address.as_ref(), 465)
        .implicit_tls(true)
        .credentials(credentials)
        .connect()
        .await
        .expect("邮箱服务器连接或身份验证失败")
        .send(email)
        .await
        .expect("邮件发送失败");
}
