/*
 * @Author: chengpengfei
 * @Date: 2023-02-01 14:21:53
 * @LastEditors: chengpengfei
 * @LastEditTime: 2023-02-01 14:25:51
 * @Description:
 */
use std::{fs::File, io::Read};

pub fn get_font_data() -> Vec<u8> {
    let mut buffer = Vec::new();
    File::open("C:\\Windows\\Fonts\\msyh.ttc")
        .expect("打开微软雅黑字体失败")
        .read_to_end(&mut buffer)
        .expect("写入buffer失败");
    return buffer;
}
