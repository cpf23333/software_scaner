/*
 * @Author: chengpengfei
 * @Date: 2023-01-29 17:14:38
 * @LastEditors: chengpengfei
 * @LastEditTime: 2023-02-02 14:11:13
 * @Description:
 */
use registry::{self, Security};

use crate::types::structs::Table;

pub fn get_reg_data() -> Table {
    let sub_key = "software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall";
    let regkey = registry::Hive::LocalMachine
        .open(sub_key, Security::Read)
        .expect("注册表打开失败");
    fn get_reg_value(reg_key: &registry::key::RegKey, item_name: &str) -> String {
        return match reg_key.value(item_name) {
            Ok(data) => data.to_string(),
            Err(_err) => {
                // println!("给{:?}读取{}属性时失败：{:?}",reg_key.to_string(),item_name, err);
                "".to_string()
            }
        };
    }
    let head = vec![
        "软件名称".to_string(),
        "软件版本".to_string(),
        "发布者/发布公司".to_string(),
        "安装时间".to_string(),
    ];
    let mut body = vec![];
    regkey.keys().for_each(|key| {
        let data = key.unwrap().open(Security::Read).expect(&format!(""));
        let display_name = get_reg_value(&data, "DisplayName");
        let display_version = get_reg_value(&data, "DisplayVersion");
        let publisher = get_reg_value(&data, "Publisher");
        let install_date = get_reg_value(&data, "InstallDate");
        if display_name.len() == 0
            && display_version.len() == 0
            && publisher.len() == 0
            && install_date.len() == 0
        {
            //  todo!()
        } else {
            body.push(vec![display_name, display_version, publisher, install_date]);
        }
    });
    return Table { head, body };
}
