/*
 * @Author: chengpengfei
 * @Date: 2023-02-03 09:31:32
 * @LastEditors: chengpengfei
 * @LastEditTime: 2023-02-06 09:41:10
 * @Description:
 */
use byte_unit;
use sys_info;
use systemstat::{self, Platform};
use time::{self, format_description};
fn size_format(byte_count: u64) -> String {
    return byte_unit::Byte::from_bytes((byte_count * 1024) as u128)
        .get_appropriate_unit(true)
        .to_string();
}
#[derive(Debug)]
pub struct SysInfo {
    pub hostname: String,
    pub disk_total: String,
    pub disk_free: String,
    pub cpu_num: i32,
    pub cpu_speed: String,
    pub mem_total: String,
    pub mem_free: String,
    pub boot_time: String,
}
pub fn get_sys_info() -> SysInfo {
    let system = systemstat::System::new();
    let time = system.boot_time().unwrap();
    let format_str = "[year]年[month]月[day]日 [hour]:[minute]:[second]";
    let time = time
        .format(&format_description::parse(format_str).unwrap())
        .unwrap();
    let disk_info = sys_info::disk_info().expect("读取磁盘信息失败");
    // println!("diskinfo{:?}", disk_info);
    let hostname = match sys_info::hostname() {
        Ok(str) => str,
        Err(_) => "计算机名称读取失败".to_string(),
    };
    let mem_info = sys_info::mem_info().expect("内存信息读取失败");
    let cpu_num = sys_info::cpu_num().expect("cpu数量读取失败");
    let cpu_speed = sys_info::cpu_speed().expect("cpu速度读取失败");
    return SysInfo {
        hostname,
        disk_total: size_format(disk_info.total),
        disk_free: size_format(disk_info.free),
        cpu_num: cpu_num as i32,
        cpu_speed: cpu_speed.to_string() + "Mhz",
        mem_total: size_format(mem_info.total),
        mem_free: size_format(mem_info.free),
        boot_time: time,
    };
}
