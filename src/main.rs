/*
 * @Author: chengpengfei
 * @Date: 2022-08-17 10:44:40
 * @LastEditors: chengpengfei
 * @LastEditTime: 2023-02-03 17:18:27
 * @Description:
 */
// #![allow(unused)]

use utils::{
    // ask_email_about::ask_info,
     get_reg::get_reg_data,
      get_sys_info::get_sys_info,
    // send_email::send_email,
     utils::get_save_file_path,
};
mod by_html;
// mod by_pdf;
mod types;
mod utils;
// #[tokio::main]
fn main() {
    let data = get_reg_data();
    let path = get_save_file_path();
    let sys_info = get_sys_info();
    // println!("{:?}", sys_info);
    by_html::run(data, Some(path), Some(sys_info))
    // let _buffer = by_pdf::run(data, path);
    // let info = ask_info();
    // send_email(buffer,&info).await;
    // println!("邮件发送成功")
}
