/*
 * @Author: chengpengfei
 * @Date: 2023-01-30 13:44:16
 * @LastEditors: chengpengfei
 * @LastEditTime: 2023-02-14 14:49:16
 * @Description:
 */
#[derive(Debug)]
pub struct Table {
    /**表头 */
    pub(crate) head: Vec<String>,
    /**表体 */
    pub(crate) body: Vec<Vec<String>>,
}
#[derive(Debug)]
pub(crate) struct ColWidthPercent {
    // pub(crate) total_width: usize,
    // pub(crate) widths: Vec<usize>,
    // pub(crate) percent: Vec<f32>,
}
impl Table {
//    /**获取总宽度，各列宽度及各宽度百分比 */
    // pub(crate) fn get_col_width_percent(
    //     &self,
    //     counter: &dyn Fn(String) -> usize,
    // ) -> ColWidthPercent {
    //     let mut widths = vec![];
    //     let mut col_index = 0;
    //     let mut index = 0;
    //     for title in &self.head {
    //         let mut max_len = counter(title.to_string());
    //         let mut current_col_max_len = max_len.clone();
    //         for line in &self.body {
    //             let cell_len = counter(line[index].to_string());
    //             if cell_len > max_len {
    //                 max_len = cell_len;
    //             }
    //             let current_cell_len = counter(line[col_index].to_string());
    //             if current_cell_len > current_col_max_len {
    //                 current_col_max_len = current_cell_len;
    //             }
    //         }
    //         index += 1;
    //         widths.push(current_col_max_len);
    //         col_index = 1 + col_index;
    //     }
    //     return ColWidthPercent {
    //         // total_width: row_max_width,
    //         widths,
    //         // percent,
    //     };
    // }
}
